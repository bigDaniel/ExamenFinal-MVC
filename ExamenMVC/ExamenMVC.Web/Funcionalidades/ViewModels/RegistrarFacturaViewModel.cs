﻿using System;
using System.ComponentModel.DataAnnotations;
using FluentValidation.Attributes;

namespace ExamenMVC.Web.Funcionalidades.ViewModels
{
    [Validator(typeof(RegistrarFacturaViewModelValidator))]
    public class RegistrarFacturaViewModel
    {
        public int Numero { get; set; }

        [Display(Name = "Fecha Emisión")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "0:dd/MM/yyyy", ApplyFormatInEditMode = true)]
        public DateTime FechaEmision { get; set; }

        [Display(Name = "Fecha Vencimiento")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "0:dd/MM/yyyy", ApplyFormatInEditMode = true)]
        public DateTime FechaVencimiento { get; set; }

        [Display(Name = "Fecha Cobro")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "0:dd/MM/yyyy", ApplyFormatInEditMode = true)]
        public DateTime FechaCobro { get; set; }


        [Display(Name = "RUC")]
        public string RucCliente { get; set; }
        [Display(Name = "Razón Social")]
        public string RazonSocialCliente { get; set; }
        [Display(Name = "Total")]
        public decimal TotalFactura { get; set; }
        [Display(Name = "Impuesto")]
        public decimal TotalImpuesto { get; set; }
        [Display(Name = "Archivo")]
        public string ImagenMini { get; set; }
        public string ImagenMiniExtension { get; internal set; }
        public int EmpresaId { get; set; }
    }
}