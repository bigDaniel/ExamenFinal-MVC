﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ExamenMVC.Web.Funcionalidades.ViewModels
{
    public class EditarFacturaViewModel
    {
        public int Numero { get; set; }

        [Display(Name = "Fecha Emisión")]
        public DateTime FechaEmision { get; set; }

        [Display(Name = "Fecha Vencimiento")]
        public DateTime FechaVencimiento { get; set; }

        [Display(Name = "Fecha Cobro")]
        public DateTime FechaCobro { get; set; }
        
        [Display(Name = "RUC")]
        public string RucCliente { get; set; }
        [Display(Name = "Razón Social")]
        public string RazonSocialCliente { get; set; }
        [Display(Name = "Total")]
        public decimal TotalFactura { get; set; }
        [Display(Name = "Impuesto")]
        public decimal TotalImpuesto { get; set; }
        [Display(Name = "Archivo")]
        public string ImagenMini { get; set; }
        public string ImagenMiniExtension { get; internal set; }
        public int EmpresaId { get; set; }
    }
}