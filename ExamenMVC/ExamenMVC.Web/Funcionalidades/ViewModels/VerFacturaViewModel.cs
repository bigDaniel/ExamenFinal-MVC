﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ExamenMVC.Web.Funcionalidades.ViewModels
{
    public class VerFacturaViewModel
    {
        public int Numero { get; set; }
        [Display(Name = "Fecha Emisión")]
        public DateTime FechaEmision { get; set; }
        [Display(Name = "RUC Cliente")]
        public string RucCliente { get; set; }
        [Display(Name = "Razón Social")]
        public string RazonSocialCliente { get; set; }
        [Display(Name = "Total")]
        public decimal TotalFactura { get; set; }
        [Display(Name = "Impuesto")]
        public decimal TotalImpuesto { get; set; }
        public int EmpresaId { get; set; }
    }
}