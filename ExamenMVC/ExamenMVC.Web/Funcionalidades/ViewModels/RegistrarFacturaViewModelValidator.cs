﻿using System;
using FluentValidation;

namespace ExamenMVC.Web.Funcionalidades.ViewModels
{
    public class RegistrarFacturaViewModelValidator : AbstractValidator<RegistrarFacturaViewModel>
    {
        public RegistrarFacturaViewModelValidator()
        {
            RuleFor(model => model.TotalImpuesto)
                .Must(ValidaImpuesto)
                .WithMessage("IGV inválido");

            RuleFor(model => model.FechaEmision)
                .LessThanOrEqualTo(x => x.FechaVencimiento)
                .WithMessage("La fecha de Emision debe ser menor o igual que la fecha de vencimiento");

            RuleFor(model => model.FechaVencimiento)
                .LessThanOrEqualTo(x => x.FechaCobro)
                .WithMessage("La fecha de vencimiento debe ser menor o igual que la fecha de cobro");

            RuleFor(model => model.TotalFactura)
                .GreaterThan(0)
                .WithMessage("Total debe ser mayor que cero");
            
        }

        private bool ValidaImpuesto(RegistrarFacturaViewModel modelo, decimal igv)
        {
            return (igv == (modelo.TotalFactura * 0.18m));
        }
    }
}