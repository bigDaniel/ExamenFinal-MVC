﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamenMVC.Web.Funcionalidades.ViewModels
{
    public class ListaEmpresasViewModel
    {
        public int Id { get; set; }
        public string Ruc { get; set; }        
        public string RazonSocial { get; set; }
        public string Direccion { get; set; }
        public string Departamento { get; set; }
        public string Rubro { get; set; }
    }
}