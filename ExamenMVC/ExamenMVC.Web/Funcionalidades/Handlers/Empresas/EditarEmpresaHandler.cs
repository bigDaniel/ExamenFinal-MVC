﻿using ExamenMVC.Aplicacion;
using ExamenMVC.Repositorio.Impl;
using ExamenMVC.Web.Funcionalidades.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamenMVC.Web.Funcionalidades.Handlers.Empresas
{
    public class EditarEmpresaHandler : IDisposable
    {
        public readonly FactoryDBRepositorio repo;
        public EditarEmpresaHandler()
        {
            repo = new FactoryDBRepositorio(new FacturaDB());
        }

        public RegistrarEmpresaViewModel Execute(int id)
        {
            var empresa = repo.Empresas.TraerUno(x => x.Id == id);
            return new RegistrarEmpresaViewModel()
            {
                Id = empresa.Id,
                Ruc = empresa.Ruc,
                RazonSocial = empresa.RazonSocial,
                Direccion = empresa.Direccion,
                Departamento = empresa.Departamento,
                Provincia = empresa.Provincia,
                Distrito = empresa.Distrito,
                Rubro = empresa.Rubro
            };
        }

        public void Actualizar(RegistrarEmpresaViewModel modelo)
        {
            repo.Empresas.Actualizar(new Empresa
            {
                Id = modelo.Id,
                Ruc = modelo.Ruc,
                RazonSocial = modelo.RazonSocial,
                Direccion = modelo.Direccion,
                Departamento = modelo.Departamento,
                Provincia = modelo.Provincia,
                Distrito = modelo.Distrito,
                Rubro = modelo.Rubro
            });
            repo.Commit();
        }

        public void Dispose()
        {
            repo.Dispose();
        }
    }
}