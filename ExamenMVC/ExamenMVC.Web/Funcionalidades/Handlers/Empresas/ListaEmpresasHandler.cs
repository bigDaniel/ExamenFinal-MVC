﻿using ExamenMVC.Aplicacion;
using ExamenMVC.Repositorio.Impl;
using ExamenMVC.Web.Funcionalidades.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamenMVC.Web.Funcionalidades.Handlers.Empresas
{
    public class ListaEmpresasHandler : IDisposable
    {
        public readonly FactoryDBRepositorio repo;
        public ListaEmpresasHandler()
        {
            repo = new FactoryDBRepositorio(new FacturaDB());
        }
        public void Dispose()
        {
            repo.Dispose();
        }

        public IEnumerable<ListaEmpresasViewModel> EjecutarLista()
        {
            return repo.Empresas.TraerTodos().Select(e => new ListaEmpresasViewModel()
            {
                Id = e.Id,
                Ruc = e.Ruc,
                RazonSocial = e.RazonSocial,
                Direccion = e.Direccion,
                Departamento = e.Departamento,
                Rubro = e.Rubro
            }).ToList();
        }
    }
}