﻿using ExamenMVC.Aplicacion;
using ExamenMVC.Repositorio.Impl;
using ExamenMVC.Web.Funcionalidades.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamenMVC.Web.Funcionalidades.Handlers.Empresas
{
    public class EliminarEmpresaHandler : IDisposable
    {
        private readonly FactoryDBRepositorio repo;
        private readonly FacturaDB db;
        public EliminarEmpresaHandler()
        {
            db = new FacturaDB();
            repo = new FactoryDBRepositorio(db);
        }

        public ListaEmpresasViewModel Execute(int id)
        {
            var empresa = repo.Empresas.TraerUno(x => x.Id == id);

            return new ListaEmpresasViewModel()
            {
                Id = empresa.Id,
                Ruc = empresa.Ruc,
                RazonSocial = empresa.RazonSocial,
                Direccion = empresa.Direccion                                
            };
        }

        public void Eliminar(ListaEmpresasViewModel modelo)
        {
            var empresa = repo.Empresas.TraerUno(x => x.Id == modelo.Id);
            var facturas = empresa.Facturas.Count();
            if (facturas > 0) throw new Exception("Operación inválida.Hay facturas amarradas a esta empresa");
            else
            {
                db.Empresas.Remove(empresa);
                db.SaveChanges();
            }
        }

        public void Dispose()
        {
            repo.Dispose();
        }
    }
}