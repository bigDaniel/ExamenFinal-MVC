﻿using ExamenMVC.Aplicacion;
using ExamenMVC.Repositorio.Impl;
using ExamenMVC.Web.Funcionalidades.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamenMVC.Web.Funcionalidades.Handlers.Empresas
{
    public class RegistrarEmpresaHandler : IDisposable
    {
        public readonly FactoryDBRepositorio repo;
        public readonly FacturaDB db;
        public RegistrarEmpresaHandler()
        {
            db = new FacturaDB();
            repo = new FactoryDBRepositorio(db);
        }

        public void Agregar(RegistrarEmpresaViewModel modelo)
        {
            bool valido = ValidaRUC(modelo.Ruc);
            if (valido == false) throw new Exception("RUC no válido");
            if (!db.Empresas.Any(z => z.RazonSocial == modelo.RazonSocial))
            {
                Empresa entidad = CrearEmpresa(modelo);
                repo.Empresas.Agregar(entidad);
                repo.Commit();
            }
            else { throw new Exception("Razón social ya existe"); }
                
        }

        private bool ValidaRUC(string ruc)
        {
            //MODULO 11
            //if (Int32.Parse(ruc.ToString()) > 0) throw new Exception("EL valor no es numerico");
            if (ruc.Length != 11) throw new Exception("Numero de digitos invalido");

            int dig01 = Convert.ToInt32(ruc.Substring(0, 1)) * 5;
            int dig02 = Convert.ToInt32(ruc.Substring(1, 1)) * 4;
            int dig03 = Convert.ToInt32(ruc.Substring(2, 1)) * 3;
            int dig04 = Convert.ToInt32(ruc.Substring(3, 1)) * 2;
            int dig05 = Convert.ToInt32(ruc.Substring(4, 1)) * 7;
            int dig06 = Convert.ToInt32(ruc.Substring(5, 1)) * 6;
            int dig07 = Convert.ToInt32(ruc.Substring(6, 1)) * 5;
            int dig08 = Convert.ToInt32(ruc.Substring(7, 1)) * 4;
            int dig09 = Convert.ToInt32(ruc.Substring(8, 1)) * 3;
            int dig10 = Convert.ToInt32(ruc.Substring(9, 1)) * 2;
            int dig11 = Convert.ToInt32(ruc.Substring(10, 1));

            int suma = dig01 + dig02 + dig03 + dig04 + dig05 + dig06 + dig07 + dig08 + dig09 + dig10 + dig11;
            int residuo = suma % 11;
            int resto = 11 - residuo;

            int digChk;
            if (resto == 10)
                digChk = 0;
            else if (resto == 11)
                digChk = 1;
            else
                digChk = resto;


            if (dig11 == digChk)
                return true;
            else
                return false;
        }

        private Empresa CrearEmpresa(RegistrarEmpresaViewModel modelo)
        {
            return new Empresa()
            {
                Ruc = modelo.Ruc,
                RazonSocial = modelo.RazonSocial,
                Direccion = modelo.Direccion,
                Departamento = modelo.Departamento,
                Provincia = modelo.Provincia,
                Distrito = modelo.Distrito,
                Rubro = modelo.Rubro
            };
        }

        public void Dispose()
        {
            repo.Dispose();
        }
    }
}