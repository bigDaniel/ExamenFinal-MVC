﻿using ExamenMVC.Aplicacion;
using ExamenMVC.Repositorio.Comun;
using ExamenMVC.Repositorio.Impl;
using ExamenMVC.Web.Funcionalidades.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ExamenMVC.Web.Funcionalidades.Handlers.Facturas
{
    public class RegistrarFacturaHandler : IDisposable
    {
        private readonly FactoryDBRepositorio repo;
        private readonly FacturaDB db;
        public RegistrarFacturaHandler()
        {
            db = new FacturaDB();
            repo = new FactoryDBRepositorio(db);
        }

        public void Insertar(RegistrarFacturaViewModel modelo)
        {
            bool valido = ValidaRuc(modelo.RucCliente);
            if (valido == false) throw new Exception("RUC no válido");

            var factura = CrearFactura(modelo);
            repo.Facturas.Agregar(factura);
            repo.Commit();
        }

        private bool ValidaRuc(string ruc)
        {
            //MODULO 11
            //if (Int32.Parse(ruc.ToString()) > 0) throw new Exception("EL valor no es numerico");
            if (ruc.Length != 11) throw new Exception("Numero de digitos invalido");

            int dig01 = Convert.ToInt32(ruc.Substring(0, 1)) * 5;
            int dig02 = Convert.ToInt32(ruc.Substring(1, 1)) * 4;
            int dig03 = Convert.ToInt32(ruc.Substring(2, 1)) * 3;
            int dig04 = Convert.ToInt32(ruc.Substring(3, 1)) * 2;
            int dig05 = Convert.ToInt32(ruc.Substring(4, 1)) * 7;
            int dig06 = Convert.ToInt32(ruc.Substring(5, 1)) * 6;
            int dig07 = Convert.ToInt32(ruc.Substring(6, 1)) * 5;
            int dig08 = Convert.ToInt32(ruc.Substring(7, 1)) * 4;
            int dig09 = Convert.ToInt32(ruc.Substring(8, 1)) * 3;
            int dig10 = Convert.ToInt32(ruc.Substring(9, 1)) * 2;
            int dig11 = Convert.ToInt32(ruc.Substring(10, 1));

            int suma = dig01 + dig02 + dig03 + dig04 + dig05 + dig06 + dig07 + dig08 + dig09 + dig10 + dig11;
            int residuo = suma % 11;
            int resto = 11 - residuo;

            int digChk;
            if (resto == 10)
                digChk = 0;
            else if (resto == 11)
                digChk = 1;
            else
                digChk = resto;


            if (dig11 == digChk)
                return true;
            else
                return false;
        }

        private Factura CrearFactura(RegistrarFacturaViewModel modelo)
        {
            return new Factura()
            {
                FechaEmision = modelo.FechaEmision,
                FechaVencimiento = modelo.FechaVencimiento,
                FechaCobro = modelo.FechaCobro,
                RucCliente = modelo.RucCliente,
                RazonSocialCliente = modelo.RazonSocialCliente,
                TotalFactura = modelo.TotalFactura,
                TotalImpuesto = modelo.TotalImpuesto,
                ImagenMini = modelo.ImagenMini,
                ImagenMiniExtension = modelo.ImagenMiniExtension,
                EmpresaId = modelo.EmpresaId
            };
        }

        public void GrabarImgFactura(RegistrarFacturaViewModel modelo, Imagen imagen)
        {
            if(imagen == null) { Insertar(modelo); return; }
            modelo.ImagenMini = Path.GetFileNameWithoutExtension(imagen.Nombre);
            modelo.ImagenMiniExtension = Path.GetExtension(imagen.Nombre).Replace(".", "");

            imagen.GrabaThumbnail(modelo.ImagenMini + Path.GetExtension(imagen.Nombre));
            Insertar(modelo);
        }
        

        public void Dispose()
        {
            repo.Dispose();
        }
    }
}