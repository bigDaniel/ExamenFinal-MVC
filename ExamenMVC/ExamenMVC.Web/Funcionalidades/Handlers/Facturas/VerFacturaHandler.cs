﻿using ExamenMVC.Aplicacion;
using ExamenMVC.Repositorio.Impl;
using ExamenMVC.Web.Funcionalidades.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamenMVC.Web.Funcionalidades.Handlers.Facturas
{
    public class VerFacturaHandler : IDisposable
    {
        public readonly FactoryDBRepositorio repo;
        public VerFacturaHandler()
        {
            repo = new FactoryDBRepositorio(new FacturaDB());
        }

        public IEnumerable<VerFacturaViewModel> Execute(int id)
        {
            var consulta = repo.Facturas.TraerTodos();
            if (id > 0)
                consulta = consulta.Where(x => x.EmpresaId == id);

            return consulta.Select(f => new VerFacturaViewModel()
            {
                Numero = f.Id,
                FechaEmision = f.FechaEmision,
                RucCliente = f.RucCliente,
                RazonSocialCliente = f.RazonSocialCliente,
                TotalFactura = f.TotalFactura,
                TotalImpuesto = f.TotalImpuesto
            }).ToList();
        }

        public void Dispose()
        {
            repo.Dispose();
        }
    }
}