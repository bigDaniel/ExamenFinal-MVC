﻿using ExamenMVC.Aplicacion;
using ExamenMVC.Repositorio.Impl;
using ExamenMVC.Web.Funcionalidades.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamenMVC.Web.Funcionalidades.Handlers.Facturas
{
    public class EliminarFacturaHandler : IDisposable
    {
        private readonly FactoryDBRepositorio repo;
        private readonly FacturaDB db;
        public EliminarFacturaHandler()
        {
            db = new FacturaDB();
            repo = new FactoryDBRepositorio(db);
        }

        public VerFacturaViewModel Execute(int id)
        {
            var factura = repo.Facturas.TraerUno(x => x.Id == id);
            return new VerFacturaViewModel()
            {
                Numero = factura.Id,
                FechaEmision = factura.FechaEmision,
                RazonSocialCliente = factura.RazonSocialCliente,
                RucCliente = factura.RucCliente
            };
        }

        public void EliminarFactura(VerFacturaViewModel modelo)
        {
            var factura = repo.Facturas.TraerUno(x => x.Id == modelo.Numero);
            db.Facturas.Remove(factura);
            db.SaveChanges();
        }

        public void Dispose()
        {
            repo.Dispose();
        }
    }
}