﻿using ExamenMVC.Aplicacion;
using ExamenMVC.Repositorio.Comun;
using ExamenMVC.Repositorio.Impl;
using ExamenMVC.Web.Funcionalidades.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ExamenMVC.Web.Funcionalidades.Handlers.Facturas
{
    public class EditarFacturaHandler : IDisposable
    {
        public readonly FactoryDBRepositorio repo;
        public EditarFacturaHandler()
        {
            repo = new FactoryDBRepositorio(new FacturaDB());
        }

        public EditarFacturaViewModel Execute(int id)
        {
            var factura = repo.Facturas.TraerUno(x => x.Id == id);
            return new EditarFacturaViewModel()
            {
                Numero = factura.Id,
                FechaEmision = factura.FechaEmision,
                FechaVencimiento = factura.FechaVencimiento,
                FechaCobro = factura.FechaCobro,
                RucCliente = factura.RucCliente,
                RazonSocialCliente = factura.RazonSocialCliente,
                TotalFactura = factura.TotalFactura,
                TotalImpuesto = factura.TotalImpuesto,
                EmpresaId = factura.EmpresaId
            };
        }

        public void ActualizarFactura(EditarFacturaViewModel modelo)
        {
            repo.Facturas.Actualizar(new Factura
            {
                Id = modelo.Numero,
                FechaEmision = modelo.FechaEmision,
                FechaVencimiento = modelo.FechaVencimiento,
                FechaCobro = modelo.FechaCobro,
                RucCliente = modelo.RucCliente,
                RazonSocialCliente = modelo.RazonSocialCliente,
                TotalFactura = modelo.TotalFactura,
                TotalImpuesto = modelo.TotalImpuesto,
                ImagenMini = modelo.ImagenMini,
                ImagenMiniExtension = modelo.ImagenMiniExtension,
                EmpresaId = modelo.EmpresaId
            });
            repo.Commit();
        }

        public void GrabarImgFactura(EditarFacturaViewModel modelo, Imagen imagen)
        {
            if (imagen == null) { ActualizarFactura(modelo); return; }
            modelo.ImagenMini = Path.GetFileNameWithoutExtension(imagen.Nombre);
            modelo.ImagenMiniExtension = Path.GetExtension(imagen.Nombre).Replace(".", "");

            imagen.GrabaThumbnail(modelo.ImagenMini + Path.GetExtension(imagen.Nombre));
            ActualizarFactura(modelo);
        }

        public void Dispose()
        {
            repo.Dispose();
        }

    }
}