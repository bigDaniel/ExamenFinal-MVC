// <auto-generated />
namespace ExamenMVC.Web.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class usuario : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(usuario));
        
        string IMigrationMetadata.Id
        {
            get { return "201612211456035_usuario"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
