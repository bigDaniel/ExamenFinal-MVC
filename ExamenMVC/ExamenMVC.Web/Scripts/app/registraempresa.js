﻿var moduloPersona = (function () {
    function configuraCombosEnCascadaUbigeo($seccion) {
        if ($seccion.length <= 0) return; //si la seccion no existe o esta vacia

        //configurar
        $seccion.cascadingDropdown(
            {
                selectBoxes: [
                    {
                        selector: 'select.departamento',
                        source: function (request, response) {
                            $.getJSON('/Ubigeo/Departamentos', function (dptosLista) {
                                var dpto = $seccion.find('input[type=hidden].departamento').val(); // para preseleccionar el depto.
                                // convierte la lista de dptos en una lista de objetos para el combo
                                var dptos = $.map(dptosLista, function (item, index) {
                                    return {
                                        label: item,
                                        value: item,
                                        selected: item === dpto
                                    }
                                });

                                response(dptos);

                            });
                        }
                    },
                    {
                        selector: 'select.provincia',
                        requires: ['select.departamento'],
                        source: function (request, response) {
                            $.getJSON('/Ubigeo/Provincias', request,
                              function (provsLista) {
                                var prov = $seccion.find('input[type=hidden].provincia').val();
                                var provs = $.map(provsLista, function (item, index) {
                                    return {
                                        label: item,
                                        value: item,
                                        selected: item === prov
                                    }
                                });

                                response(provs);

                            });
                        }
                    },
                    {
                        selector: 'select.distrito',
                        requires: ['select.departamento','select.provincia'],
                        source: function (request, response) {
                            $.getJSON('/Ubigeo/Distritos', request,
                              function (distsLista) {
                                var dist = $seccion.find('input[type=hidden].distrito').val();
                                var dists = $.map(distsLista, function (item, index) {
                                    return {
                                        label: item,
                                        value: item,
                                        selected: item === dist
                                    }
                                });

                                response(dists);

                            });
                        },
                        onChange: function (event, value, requiredValues) {
                            if (value) {
                                $seccion.find('input[type=hidden].departamento').val(requiredValues['departamento'])
                                $seccion.find('input[type=hidden].provincia').val(requiredValues['provincia'])
                                $seccion.find('input[type=hidden].distrito').val(value)
                            }
                        }
                    }                    
                ]
            });        
    }

    //publicamos los metodos del modulo
    return {
        vincularControles: function () {
            configuraCombosEnCascadaUbigeo($("#empresa-ubigeo"));
        }
    }

})();



//configuramos la IIEF
$(function () {
    moduloPersona.vincularControles();
});