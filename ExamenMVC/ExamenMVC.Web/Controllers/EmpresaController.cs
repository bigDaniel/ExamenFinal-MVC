﻿using ExamenMVC.Web.Funcionalidades.Handlers.Empresas;
using ExamenMVC.Web.Funcionalidades.ViewModels;
using IdentitySample.Models;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExamenMVC.Web.Controllers
{
    public class EmpresaController : Controller
    {
        // GET: Empresa
        public ActionResult Lista()
        {
            using (var lista = new ListaEmpresasHandler())
            {
                return View(lista.EjecutarLista());
            }          
        }

        [HttpGet]
        public ActionResult Registrar()
        {
            return View(new RegistrarEmpresaViewModel());
        }

        [HttpPost]
        public ActionResult Registrar(RegistrarEmpresaViewModel modelo)
        {
            if (!ModelState.IsValid) return View(modelo);
            using (var registrar = new RegistrarEmpresaHandler())
            {
                try
                {
                    //inserta
                    registrar.Agregar(modelo);
                    return RedirectToAction("Lista");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(modelo);
                }
            }
        }

        [HttpGet]
        public PartialViewResult Editar(int id)
        {            
            using (var edit = new EditarEmpresaHandler())
            {
                return PartialView("_Editar", edit.Execute(id));
            }
        }

        [HttpPost]
        public ActionResult Editar(RegistrarEmpresaViewModel modelo)
        {
            if (!ModelState.IsValid) return View(modelo);
            using (var actualiza = new EditarEmpresaHandler())
            {
                try
                {
                    actualiza.Actualizar(modelo);
                    return RedirectToAction("Lista");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(modelo);
                }
            }
        }

        [HttpGet]
        public PartialViewResult Eliminar(int id)
        {
            using (var ver = new EliminarEmpresaHandler())
            {
                return PartialView("Eliminar", ver.Execute(id));
            }
        }

        [HttpPost]
        public ActionResult Eliminar(ListaEmpresasViewModel modelo)
        {
            if (!ModelState.IsValid) return View(modelo);
            using (var elimina = new EliminarEmpresaHandler())
            {                                
                try
                {
                    elimina.Eliminar(modelo);                    
                    return RedirectToAction("Lista", "Empresa");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View("ErrorEliminar");
                }
            }
        }

        public PartialViewResult ErrorEliminar()
        {                     
            return PartialView("ErrorEliminar", new ListaEmpresasViewModel());                        
        }
    }
}