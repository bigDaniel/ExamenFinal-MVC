﻿using ExamenMVC.Repositorio.Comun;
using ExamenMVC.Web.Funcionalidades.Handlers.Facturas;
using ExamenMVC.Web.Funcionalidades.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExamenMVC.Web.Controllers
{
    public class FacturaController : Controller
    {
        // GET: Factura
        public ActionResult Ver(int id)
        {
            using (var list = new VerFacturaHandler())
            {
                return View(list.Execute(id));
            }
        }
        
        [HttpGet]
        public ActionResult Registrar(int id)
        {
            ViewBag.Id = id;
            return View(new RegistrarFacturaViewModel());
        }

        [HttpPost]
        public ActionResult Registrar(RegistrarFacturaViewModel modelo, int id, HttpPostedFileBase image)
        {
            string ruta = Server.MapPath("~/imagenes/Facturas");
            ViewBag.Id = id;
            modelo.EmpresaId = ViewBag.Id;            
            if (!ModelState.IsValid) return View(modelo);
            using (var registra = new RegistrarFacturaHandler())
            {
                try
                {
                    Imagen imagen = ObtenerImagen(image, ruta);
                    registra.GrabarImgFactura(modelo, imagen);
                    return RedirectToAction("Ver", "Factura", new { id = ViewBag.Id });
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(modelo);
                }
            }
        }

        private Imagen ObtenerImagen(HttpPostedFileBase image, string ruta)
        {
            if (image == null) return null;
            var stream = new MemoryStream();

            image.InputStream.CopyTo(stream);
            return new Imagen(stream, image.FileName, image.ContentType, ruta);
        }

        [HttpGet]
        public PartialViewResult Editar(int id)
        {
            using (var buscar = new EditarFacturaHandler())
            {
                TempData["empid"] = buscar.Execute(id).EmpresaId;
                return PartialView("Editar", buscar.Execute(id));
            }
        }

        [HttpPost]
        public ActionResult Editar(EditarFacturaViewModel modelo, HttpPostedFileBase image)
        {
            string ruta = Server.MapPath("~/imagenes/Facturas");
            modelo.EmpresaId = Convert.ToInt32(TempData["empid"]);
            if (!ModelState.IsValid) return View(modelo);
            using (var edita = new EditarFacturaHandler())
            {
                try
                {
                    Imagen imagen = ObtenerImagen(image, ruta);
                    edita.GrabarImgFactura(modelo, imagen);
                    return RedirectToAction("Lista", "Empresa");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(modelo);
                }
            }
        }

        [HttpGet]
        public PartialViewResult Eliminar(int id)
        {
            using (var ver = new EliminarFacturaHandler())
            {
                return PartialView("Eliminar", ver.Execute(id));
            }
        }

        [HttpPost]
        public ActionResult Eliminar(VerFacturaViewModel modelo)
        {
            if (!ModelState.IsValid) return View(modelo);
            using (var elimina = new EliminarFacturaHandler())
            {
                try
                {
                    elimina.EliminarFactura(modelo);
                    return RedirectToAction("Lista", "Empresa");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(modelo);
                }
            }
        }
    }
}