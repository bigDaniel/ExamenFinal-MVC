﻿using ExamenMVC.Web.Funcionalidades.Handlers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ExamenMVC.Web.Controllers
{
    public class UbigeoController : Controller
    {
        private UbigeoHelper _ubigeo;

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            _ubigeo = new UbigeoHelper(Path.Combine(Server.MapPath("~/App_Data"), "Ubigeo.xml"));
        }
        // GET: Ubigeo
        [HttpGet]
        public JsonResult Departamentos()
        {
            return Json(_ubigeo.GetDepartamentos().ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Provincias(string departamento)
        {
            return Json(_ubigeo.GetProvincias(departamento).ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Distritos(string departamento, string provincia)
        {
            return Json(_ubigeo.GetDistritos(departamento, provincia).ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}