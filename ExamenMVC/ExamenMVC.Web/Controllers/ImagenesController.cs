﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExamenMVC.Web.Controllers
{
    public class ImagenesController : Controller
    {
        // GET: Imagenes
        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(CacheProfile = "FacturaImages")]
        public FileResult Show(string id, string ext)
        {
            var path = Path
                .Combine(Server.MapPath("~/Imagenes/Facturas"), id + $".{ext}");
            return new FileStreamResult(
                    new FileStream(path, FileMode.Open),
                    MimeMapping.GetMimeMapping($"{id}.{ext}"));
        }
    }
}