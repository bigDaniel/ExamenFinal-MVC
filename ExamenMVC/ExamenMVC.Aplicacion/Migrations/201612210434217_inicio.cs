namespace ExamenMVC.Aplicacion.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inicio : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Empresa",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ruc = c.String(),
                        RazonSocial = c.String(nullable: false),
                        Direccion = c.String(),
                        Departamento = c.String(),
                        Provincia = c.String(),
                        Distrito = c.String(),
                        Rubro = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Factura",
                c => new
                    {
                        Numero = c.String(nullable: false, maxLength: 128),
                        FechaEmision = c.DateTime(nullable: false),
                        FechaVencimiento = c.DateTime(nullable: false),
                        FechaCobro = c.DateTime(nullable: false),
                        RucCliente = c.String(),
                        RazonSocialCliente = c.String(),
                        TotalFactura = c.String(),
                        TotalImpuesto = c.String(),
                        ImagenMini = c.String(),
                        ImagenMiniExtension = c.String(),
                        EmpresaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Numero)
                .ForeignKey("dbo.Empresa", t => t.EmpresaId, cascadeDelete: true)
                .Index(t => t.EmpresaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Factura", "EmpresaId", "dbo.Empresa");
            DropIndex("dbo.Factura", new[] { "EmpresaId" });
            DropTable("dbo.Factura");
            DropTable("dbo.Empresa");
        }
    }
}
