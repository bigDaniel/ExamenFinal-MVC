namespace ExamenMVC.Aplicacion.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class numerofac : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Factura");
            AddColumn("dbo.Factura", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Factura", "Id");
            DropColumn("dbo.Factura", "Numero");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Factura", "Numero", c => c.Int(nullable: false, identity: true));
            DropPrimaryKey("dbo.Factura");
            DropColumn("dbo.Factura", "Id");
            AddPrimaryKey("dbo.Factura", "Numero");
        }
    }
}
