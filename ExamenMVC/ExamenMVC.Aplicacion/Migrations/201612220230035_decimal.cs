namespace ExamenMVC.Aplicacion.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _decimal : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Factura", "TotalFactura", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Factura", "TotalImpuesto", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Factura", "TotalImpuesto", c => c.String());
            AlterColumn("dbo.Factura", "TotalFactura", c => c.String());
        }
    }
}
