﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenMVC.Aplicacion
{
    public class Empresa
    {
        public Empresa()
        {
            Facturas = new HashSet<Factura>();
        }
        
        public int Id { get; set; }
        public string Ruc { get; set; }
        [Required]
        public string RazonSocial { get; set; }
        public string Direccion { get; set; }
        public string Departamento { get; set; }
        public string Provincia { get; set; }
        public string Distrito { get; set; }
        public string Rubro { get; set; }

        public virtual ICollection<Factura> Facturas { get; set; }
    }
}
