﻿using System;

namespace ExamenMVC.Aplicacion
{
    public class Factura
    {  
        public int Id { get; set; }
        public DateTime FechaEmision { get; set; }
        public DateTime FechaVencimiento { get; set; }
        public DateTime FechaCobro { get; set; }
        public string RucCliente { get; set; }
        public string RazonSocialCliente { get; set; }
        public decimal TotalFactura { get; set; }
        public decimal TotalImpuesto { get; set; }
        public string ImagenMini { get; set; }
        public string ImagenMiniExtension { get; set; }

        //empresa
        public int EmpresaId { get; set; }
        public virtual Empresa Empresa { get; set; }
    }
}
