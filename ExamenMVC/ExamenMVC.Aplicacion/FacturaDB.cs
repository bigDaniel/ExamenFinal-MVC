﻿namespace ExamenMVC.Aplicacion
{
    using Aplicacion;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using System.Linq;

    public class FacturaDB : DbContext
    {
        public FacturaDB() : base("name=FacturaDB") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder
                .Conventions
                .Remove<PluralizingTableNameConvention>();
        }

        public virtual DbSet<Empresa> Empresas { get; set; }
        public virtual DbSet<Factura> Facturas { get; set; }
    }
}