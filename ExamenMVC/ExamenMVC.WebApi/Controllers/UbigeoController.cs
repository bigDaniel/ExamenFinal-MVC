﻿using ExamenMVC.WebApi.Logica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace ExamenMVC.WebApi.Controllers
{
    public class UbigeoController : ApiController
    {
        private readonly IRepositorio repositorio;
        public UbigeoController()
        {
            repositorio = (IRepositorio)new Repositorio();
        }
        // GET: Ubigeo
        public IHttpActionResult Get()
        {
            return Ok(repositorio.Traer());
        }
    }
}