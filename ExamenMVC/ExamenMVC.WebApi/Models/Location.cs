﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamenMVC.WebApi.Models
{
    public class Location
    {
        public string Departamento { get; set; }
        public string Provincia { get; set; }
        public string Distrito { get; set; }
    }
}