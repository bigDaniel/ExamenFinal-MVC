﻿using ExamenMVC.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamenMVC.WebApi.Logica
{
    public class Repositorio
    {
        private static List<Location> _locaciones;
        public Repositorio()
        {
            if (_locaciones == null) _locaciones = new List<Location>();
        }

        public List<Location> Traer()
        {
            return _locaciones;
        }
    }
}