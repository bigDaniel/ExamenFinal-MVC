﻿using ExamenMVC.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenMVC.WebApi.Logica
{
    interface IRepositorio
    {
        List<Location> Traer();
    }
}
