﻿using ExamenMVC.Aplicacion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenMVC.Repositorio.Impl
{
    public class FactoryDBRepositorio : IDisposable
    {
        private readonly FacturaDB db;
        private readonly RepositorioGenerico<Empresa> _empresa;
        private readonly RepositorioGenerico<Factura> _factura;
        public FactoryDBRepositorio(FacturaDB db)
        {
            this.db = db;
            _empresa = new RepositorioGenerico<Empresa>(db);
            _factura = new RepositorioGenerico<Factura>(db);
        }

        public RepositorioGenerico<Empresa> Empresas { get { return _empresa; } }
        public RepositorioGenerico<Factura> Facturas { get { return _factura; } }

        public void Dispose()
        {
            db.Dispose();
        }
        public void Commit()
        {
            db.SaveChanges();
        }
    }
}
